這是一個從 Jichi 所開發 VNR 的分支版本。
---------------------------------

[主要功能]
這個版本主要目標，在於使 VNR 和 OBS 連動，
將 VNR 的翻譯的文字顯示在實況畫面上。

---------------------------------

[安裝方法]

你需要同時安裝修改後的 VNR 與 OBS 才能正常運作，
你可以使用我們提供的整包檔案，也可以選擇自行修改原先的 VNR。

---------------------------------

VNR 下載連結： https://drive.google.com/file/d/1jrQdAKVQmABOa0nPUcjh71hB5PgaHYRg/view?usp=sharing
OBS 下載連結： https://github.com/craftwar/obs-studio/releases/download/git/craftwar.obs_updater.zip

OBS 的連結是一個下載器，下載完畢後執行"update and run OBS.cmd"即可自動下載。

---------------------------------

目前我們修改以下這兩個檔案：

obs-plugins\64bit\obs-text.dll
下載連結: https://ci.appveyor.com/api/projects/craftwar_appveyor/obs-studio/artifacts/obs-text.dll?branch=master

Visual Novel Reader\Library\Frameworks\Sakura\py\apps\reader\managers\trman.py
下載連結: https://gitlab.com/craftwar/VNR_OBS/raw/master/Library/Frameworks/Sakura/py/apps/reader/managers/trman.py

如果原本就有習慣使用的 VNR 或是 OBS，也可以嘗試將這兩個檔案貼到相應位置。

---------------------------------

[使用方法]
先開啟 VNR ，在 OBS 的 obs-text 選擇 ReadFromVNR

https://i.imgur.com/qmj4s4q.png

建議設定文字自動換行，文字超過寬度時會自動在下一行顯示

https://i.imgur.com/GBZMuQu.png

Tips. 如果 VNR 忘了先開就打開選項，取消打勾重選就可以了

---------------------------------

[關於字典檔]
Caches 資料夾為字典檔的 cahce，
有些已經無法在VNR內安裝，可手動放入 Visual Novel Reader\Caches

[關於除錯]
有錯誤時可用這個腳本檢查： Visual Novel Reader\Scripts\Visual Novel Reader (Debug).cmd

[關於熱鍵錯誤]
VNR 原本的熱鍵函數庫有臭蟲存在，所以這個版本將之移除。
故熱鍵設定中按鍵需全部移除 + 關掉，否則可能會出錯
有能力的人也可以自己去改 hkman.py
(懶得改程式我直接hard code寫死設定)

[VNR]
Jichi's VNR: https://drive.google.com/drive/u/0/folders/0B3YXxE6u-4bzc1RKWHpoLWZROTQ
